# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Siga as Instruções: Importe a pasta como projeto para o Eclipse ou a IDE de sua escolha.
Compile e Execute.

Funcionamento: Cadastre um usuário e senha.
Realize os pedidos, e suas interações de pagamento e observações do cliente.

obs: Diagramas de classe e de caso de uso foram feitos na plataforma Astah.
