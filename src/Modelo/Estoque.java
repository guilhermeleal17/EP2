package Modelo;

public class Estoque {
	private String quantidade;
	private String quantidadeM;
	
	public String getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	public String getQuantidadeM() {
		return quantidadeM;
	}
	public void setQuantidadeM(String quantidadeM) {
		this.quantidadeM = quantidadeM;
	}
}
