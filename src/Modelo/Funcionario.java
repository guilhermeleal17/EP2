package Modelo;
public class Funcionario extends Pessoa {
	private String login;
	private String senha;
	
	public Funcionario(){
	}
	public String getlogin(){
		return login;
	}
	public String getSenha(){
		return senha;
	}
	public void setSenha(String senha){
		this.senha = senha;
	}
	public void setLogin(String login){
		this.login = login;
	}
}
