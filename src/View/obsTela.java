package View;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class obsTela {
	private JFrame frame;
	public JButton btnSim;
	public JButton btnNao;
	public obsTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("Restaurante");
		frame.setBounds(100, 100, 350, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JLabel lblDesej = new JLabel("Deseja fazer alguma observacao ?");
		lblDesej.setFont(new Font("Dialog", Font.ITALIC, 16));
		lblDesej.setBounds(36, 32, 302, 20);
		frame.getContentPane().add(lblDesej);
		btnSim = new JButton("Sim");
		btnSim.setBounds(61, 75, 100, 25);
		frame.getContentPane().add(btnSim);
		btnNao = new JButton("Nao");
		btnNao.setBounds(173, 75, 100, 25);
		frame.getContentPane().add(btnNao);
		frame.setVisible(true);
	}
	public void dispose() {
		frame.dispose();
	}
}
