package View;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class PagamentoTela {
	private JFrame frame;
	public JTextField nomeField;
	public JTextField cpfField;
	public RadioButtonHandler handler;
	public JRadioButton rbCartao;
	public JRadioButton rbDinheiro;
	public JButton btnCadastrar;
	public JButton btnFinalizar;
	public JButton btnCancelar;
	private JLabel lblCadastro;
	private JLabel nomeLabel;
	private JLabel lblPagamento;
	private JLabel lblCpf;
	private JSeparator separator;
	public PagamentoTela() {
		initialize();
	}
	public void initialize() {
		frame = new JFrame("Restaurante");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);		
		lblPagamento = new JLabel("Pagamento");
		lblPagamento.setFont(new Font("Dialog", Font.BOLD, 18));
		lblPagamento.setBounds(277, 30, 120, 20);
		frame.getContentPane().add(lblPagamento);	
		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.setBounds(170, 215, 105, 25);
		frame.getContentPane().add(btnFinalizar);	
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(287, 215, 105, 25);
		frame.getContentPane().add(btnCancelar);			
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(52, 215, 105, 25);
		frame.getContentPane().add(btnCadastrar);
		handler = new RadioButtonHandler();
		rbCartao = new JRadioButton("Cartao", false);
		rbCartao.setFont(new Font("Dialog", Font.BOLD, 14));
		rbCartao.setBounds(252, 147, 102, 23);
		frame.getContentPane().add(rbCartao);
		rbCartao.addItemListener(handler);
		rbDinheiro = new JRadioButton("Dinheiro", false);
		rbDinheiro.setFont(new Font("Dialog", Font.BOLD, 14));
		rbDinheiro.setBounds(252, 82, 129, 23);
		frame.getContentPane().add(rbDinheiro);
		rbDinheiro.addItemListener(handler);
		lblCadastro = new JLabel("Cadastro");
		lblCadastro.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCadastro.setBounds(64, 30, 100, 20);
		frame.getContentPane().add(lblCadastro);
		nomeLabel = new JLabel("Nome * :");
		nomeLabel.setBounds(23, 86, 70, 15);
		frame.getContentPane().add(nomeLabel);
		nomeField = new JTextField();
		nomeField.setBounds(100, 84, 114, 19);
		frame.getContentPane().add(nomeField);
		nomeField.setColumns(10);
		lblCpf = new JLabel("CPF :");
		lblCpf.setBounds(23, 141, 70, 15);
		frame.getContentPane().add(lblCpf);
		cpfField = new JTextField();
		cpfField.setColumns(10);
		cpfField.setBounds(100, 139, 114, 19);
		frame.getContentPane().add(cpfField);
		separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(229, 30, 46, 159);
		frame.getContentPane().add(separator);		
		frame.setVisible(true);
	}	
	private class RadioButtonHandler implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent event) {
			if(rbCartao.isSelected())
				rbDinheiro.setEnabled(false);
			else if(rbDinheiro.isSelected())
				rbCartao.setEnabled(false);
		}
	}
	public void dispose(){
		frame.dispose();
	}
}
