package View;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ObservacaoTela {
	private JFrame frame;
	public JTextArea observacaoArea;
	private JScrollPane scroll;
	public JButton btnCancelar;
	public JButton btnConcluir;

	public ObservacaoTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("Restaurante");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JLabel obsLabel = new JLabel("Observacoes");
		obsLabel.setFont(new Font("Dialog", Font.BOLD, 18));
		obsLabel.setBounds(150, 20, 160, 20);
		frame.getContentPane().add(obsLabel);
	    btnConcluir = new JButton("Concluir");
		btnConcluir.setBounds(101, 235, 117, 25);
		frame.getContentPane().add(btnConcluir);
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(243, 235, 117, 25);
		frame.getContentPane().add(btnCancelar);
		observacaoArea = new JTextArea();
		scroll = new JScrollPane(observacaoArea);
		scroll.setPreferredSize(new Dimension(20, 20));
		scroll.setSize(370, 162);
		scroll.setLocation(40,61);
		frame.getContentPane().add(scroll);
		observacaoArea.setLineWrap(true);
		observacaoArea.setWrapStyleWord(true);
		frame.setVisible(true);
	}

	public void dispose() {
		frame.dispose();
	}
}
