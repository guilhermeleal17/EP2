package Controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import Modelo.Produto;
import View.ProdutoTela;

public class produtoControle extends Produto{
	public ProdutoTela produtoView;
	public Produto produto;
	public produtoControle(){
		produtoView = new ProdutoTela();
		produto = new Produto();
		adicionarController(produtoView.adicionarButton);
		voltarController(produtoView.voltarButton);
	}
	public void adicionarController(JButton adicionarButton){
		adicionarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!produtoView.nomeField.getText().equals("") && !produtoView.tipoField.getText().equals("") && !produtoView.precoField.getText().equals("")){
					if(!produtoView.quantidadeField.getText().equals("") && !produtoView.quantidademField.getText().equals("")){			
							int quantity = Integer.parseInt(produtoView.quantidadeField.getText());
							int miniQuantity = Integer.parseInt(produtoView.quantidademField.getText());
						if(quantity >= miniQuantity){
							if(produtoView.tipoField.getText().equals("Prato") || produtoView.tipoField.getText().equals("Bebida") || produtoView.tipoField.getText().equals("Sobremesa")){
								try{
									BufferedWriter buff;
									buff = new BufferedWriter(new FileWriter("doc/" + produtoView.tipoField.getText() + ".txt", true));
									setTipo(produtoView.tipoField.getText());
									setNome(produtoView.nomeField.getText());
									setPreco(produtoView.precoField.getText());
									setQuantidade(produtoView.quantidadeField.getText());
									setQuantidadeM(produtoView.quantidademField.getText()); 
									String auxiliar = getNome() + ";" + getPreco() + ";" + getQuantidade() + ";" +  getQuantidadeM() + "\n";
									buff.append(auxiliar);
									buff.close();
									produtoView.dispose();
									new pedidoControle();
									
								}catch(IOException e1){
									e1.printStackTrace();
								}
							}else{
								JOptionPane.showMessageDialog(null, "Tipo de produto incorreto.");	
							}
						}else{
							JOptionPane.showMessageDialog(null, "Quantidade minima invalida.");
						}
				}else{
						JOptionPane.showMessageDialog(null, "Campo(s) nao preenchido(s).");	
						produtoView.quantidadeField.setText("");
						produtoView.quantidademField.setText(""); 
					}
				}else{
					JOptionPane.showMessageDialog(null, "Campo(s) nao preenchido(s)!");	
					produtoView.tipoField.setText("");
					produtoView.nomeField.setText("");
					produtoView.precoField.setText("");
				}
			}
		});
	}
	
	public void voltarController(JButton voltarButton){
		voltarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				produtoView.dispose();
				new pedidoControle();
			}
		});
	}
}
