package Controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import Modelo.Funcionario;
import View.CadastrarTela;

public class cadastrarControle extends Funcionario{
	public CadastrarTela cadastrarF;
	public Funcionario funcionarioModel;
	public cadastrarControle(){
		cadastrarF = new CadastrarTela();
		funcionarioModel = new Funcionario();
		finalizarController(cadastrarF.btnFinalizar);
		voltarController(cadastrarF.btnVoltar);
	}
	public void finalizarController(JButton btnFinalizar){
		btnFinalizar.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if(!cadastrarF.Nome_Field.getText().equals("") && !cadastrarF.Usuario_Field.getText().equals("")){
					if(!cadastrarF.Senha_Field.getText().equals("") && !cadastrarF.RepitaSenha_Field.getText().equals("")){
						if(cadastrarF.Senha_Field.getText().equals(cadastrarF.RepitaSenha_Field.getText())){
							try{
								BufferedWriter buff;
								buff = new BufferedWriter(new FileWriter("doc/funcionarios.txt", true));
								setLogin(cadastrarF.Usuario_Field.getText());
								setSenha(cadastrarF.Senha_Field.getText());															
								String auxiliar = getlogin() + ";" + getSenha() + "\n";								
								buff.append(auxiliar);
								buff.close();
								cadastrarF.dispose();
								new loginControle();								
							}catch(IOException e1){
								e1.printStackTrace();
							}
						}else{
							cadastrarF.RepitaSenha_Field.setText("");
							JOptionPane.showMessageDialog(null, "Senha de confirmacao incorreta!");
						}
					}else{
						JOptionPane.showMessageDialog(null, "Campos nao cadastrados!");
				     }		
				}else{
					JOptionPane.showMessageDialog(null, "Campos nao cadastrados!");
				}
			}
		});
	}
	
	public void voltarController(JButton btnVoltar){
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cadastrarF.dispose();
				new loginControle();
			}
		});
	}
	
@SuppressWarnings("deprecation")
public void cadastrar() throws IOException{						
		BufferedWriter buffWrite;
		buffWrite = new BufferedWriter(new FileWriter("doc/funcionarios.txt", true));		
		setLogin(cadastrarF.Usuario_Field.getText());
		setSenha(cadastrarF.Senha_Field.getText());
		String auxiliar = getlogin() + ";" + getSenha() + "\n";
		buffWrite.append(auxiliar);
		buffWrite.close();
	}

}
