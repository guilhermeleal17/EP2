package Controle;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import View.PedidoTela;

public class pedidoControle {
	public PedidoTela pedidoView;
	public pedidoControle(){
		pedidoView = new PedidoTela();
		inserirController(pedidoView.inserirButton);
		cancelarController(pedidoView.cancelarButton);
		pagarControler(pedidoView.pagarButton);
		sairController(pedidoView.sairButton);
		pratoController(pedidoView.pratoOk);
		bebidaController(pedidoView.bebidaOk);
		sobremesaController(pedidoView.sobremesaOk);
	}
	public void inserirController(JButton inserirButton){
		inserirButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				pedidoView.dispose();
				new produtoControle();
			}
		});
	}
	public void cancelarController(JButton cancelarButton){
		cancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pedidoView.totalArea.setText("");
				pedidoView.bebidaField.setText("");
				pedidoView.pratoField.setText("");
				pedidoView.sobremesaField.setText("");

			}
		});
	}
	public void sairController(JButton sairButton){
		sairButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pedidoView.dispose();
				new loginControle();
			}
		});
	}
	public void pagarControler(JButton pagarButton){
		pagarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pedidoView.dispose();
				new pagarControle();
			}
		});
	}
	public void pratoController(JButton pratoOk){
		pratoOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int totalPrato;
				String comboBox;
				int valorAux = 0;
				int quantidadeAux = 0;
				int quantidademAux;
				String totalP = null;
				String[] pAux = null;
				int pratoF = 0;
				
				try{
					comboBox = (String) pedidoView.pratoComboBox.getSelectedItem();
					pratoF = Integer.parseInt(pedidoView.pratoField.getText());
					BufferedReader read = new BufferedReader(new FileReader("doc/Prato.txt"));
					while(read.ready()){
						pAux = read.readLine().split(";");
						if(pAux[0].equals(comboBox)){
							valorAux = Integer.parseInt(pAux[1]);
							quantidadeAux = Integer.parseInt(pAux[2]);
							quantidademAux = Integer.parseInt(pAux[3]);
							quantidadeAux -=  pratoF;
				
							if(quantidadeAux < quantidademAux)
								JOptionPane.showMessageDialog(null, "Estoque abaixo da quantidade minima.");
							else{
								totalPrato = pratoF * valorAux;
								totalP = "" + totalPrato;
								pedidoView.totalArea.setText(pedidoView.totalArea.getText() + pAux[0] + " " + pratoF + "UN  X");
								pedidoView.totalArea.setText(pedidoView.totalArea.getText() + "    V. UN : " + valorAux + "   TOTAL : " + totalP + "\n\n");
							}
							break;
						}
					}
					try {
						read.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}catch(NumberFormatException | HeadlessException | IOException e1){
					e1.printStackTrace();
				}
			}
		});		
	}
	public void sobremesaController(JButton sobremesaOk){
		sobremesaOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int totalSobremesa;
				int valorAux = 0;
				int quantidadeAux = 0;
				int quantidademAux;
				int sobremesaF = 0;
				String comboBox;
				String totalS = null;
				String[] sAux = null;
				try{
					comboBox = (String) pedidoView.sobremesaComboBox.getSelectedItem();
					sobremesaF = Integer.parseInt(pedidoView.sobremesaField.getText());
					BufferedReader read = new BufferedReader(new FileReader("doc/Sobremesa.txt"));
					while(read.ready()){
						sAux = read.readLine().split(";");
						if(sAux[0].equals(comboBox)){
							valorAux = Integer.parseInt(sAux[1]);
							quantidadeAux = Integer.parseInt(sAux[2]);
							quantidademAux = Integer.parseInt(sAux[3]);
							quantidadeAux -=  sobremesaF;
							if(quantidadeAux < quantidademAux)
								JOptionPane.showMessageDialog(null, "Estoque abaixo da quantidade minima!");
							else{
								totalSobremesa = sobremesaF * valorAux;
								totalS = "" + totalSobremesa;
								pedidoView.totalArea.setText(pedidoView.totalArea.getText() + sAux[0] + " " + sobremesaF + "UN  X");
								pedidoView.totalArea.setText(pedidoView.totalArea.getText()+ "    V. UN : " + valorAux + "   TOTAL : " + totalS + "\n\n");
							}
							break;
						}
					}
					try {
						read.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}catch(NumberFormatException | HeadlessException | IOException e1){
					e1.printStackTrace();
				}
				
			}
		});
	}
	
	public void bebidaController(JButton bebidaOk){
		bebidaOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int totalBebida;
				int valorAux = 0;
				int quantidadebAux = 0;
				int quantidadebmAux;
				int bebidaF = 0;
				String bcomboBox;
				String totalB = null;
				String[] bAux = null;
				try{
					bcomboBox = (String) pedidoView.bebidaComboBox.getSelectedItem();
					bebidaF = Integer.parseInt(pedidoView.bebidaField.getText());
					BufferedReader read = new BufferedReader(new FileReader("doc/Bebida.txt"));
					
					while(read.ready()){
						bAux = read.readLine().split(";");
						if(bAux[0].equals(bcomboBox)){
							valorAux = Integer.parseInt(bAux[1]);
							quantidadebAux = Integer.parseInt(bAux[2]);
							quantidadebmAux = Integer.parseInt(bAux[3]);
							quantidadebAux -=  bebidaF;
							if(quantidadebAux < quantidadebmAux){
								JOptionPane.showMessageDialog(null, "Estoque abaixo da quantidade mínima!");
							}else{
								totalBebida = bebidaF * valorAux;
								totalB = "" + totalBebida;
								pedidoView.totalArea.setText(pedidoView.totalArea.getText() + bAux[0] + " " + bebidaF + "UN X");
								pedidoView.totalArea.setText(pedidoView.totalArea.getText()+ "    V. UN : " + valorAux + "   TOTAL : " + totalB + "\n\n" );
							}
							break;
						}
					}
					try {
						read.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}catch(NumberFormatException | HeadlessException | IOException e1){
					e1.printStackTrace();
				}
			}
		});
	}
}
