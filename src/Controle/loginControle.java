package Controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import Modelo.Funcionario;
import View.LoginTela;

public class loginControle extends Funcionario{
	public LoginTela loginView;
	public Funcionario funcionarioModel;
	public loginControle(){
		funcionarioModel = new Funcionario();
		loginView = new LoginTela();		
		
		entrarController(loginView.btnEntrar);
		cadastrarController(loginView.btnCadastrar);
		sairController(loginView.btnSair);
	}
	public void entrarController(JButton btnEntrar){
		btnEntrar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				try{
					verificar();
				}catch(IOException e1){
					e1.printStackTrace();
				}
			}
		});
	}
	public void cadastrarController(JButton btnCadastrar){
		btnCadastrar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				new cadastrarControle();
				loginView.dispose();
			}
		});
	}
	public void sairController(JButton btnSair){
		btnSair.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				loginView.dispose();
			}
		});
	}
	@SuppressWarnings("deprecation")
	public void verificar() throws IOException{				
		BufferedReader Reader;
		Reader = new BufferedReader(new FileReader("doc/funcionarios.txt"));
		setLogin(loginView.Usuario_Field.getText());
		setSenha(loginView.Senha_Login.getText());
		String logar = getlogin() + ";" + getSenha();
		String loginAux;
		loginAux = Reader.readLine();
		while(loginAux != null){
			if(loginAux.equals(logar)){
				new pedidoControle();
				loginView.dispose();
				break;
			}
			loginAux = Reader.readLine();
		}
		if(loginAux == null){
			loginView.Usuario_Field.setText("");
			loginView.Senha_Login.setText("");
			JOptionPane.showMessageDialog(null, "Login Invalido!\n");
		}
		Reader.close();
	}
}
