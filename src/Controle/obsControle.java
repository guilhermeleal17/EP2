package Controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import View.obsTela;

public class obsControle {
	public obsTela obsView;
	public obsControle(){
		obsView = new obsTela();
		simController(obsView.btnSim);
		naoController(obsView.btnNao);
	}
	public void simController(JButton btnSim){
		btnSim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obsView.dispose();
				new observacaoControle();
			}
		});
	}
	public void naoController(JButton btnNao){
		btnNao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obsView.dispose();
				new pedidoControle();
			}
		});
	}
}
